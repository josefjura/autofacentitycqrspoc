﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class MemoryDbContent
    {
        public string SimpleValue { get; set; }

        public List<string> SimpleList { get; set; }
    }
}
