﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace AutofacEntityCqrsPoc
{
    public class MemoryDbConnection : IDisposable
    {
        private bool isClosed = false;
        private MemoryDbContent _content;

        public MemoryDbConnection(MemoryDbContent content)
        {
            this._content = content;
        }

        public void Dispose()
        {
            this.isClosed = true;
        }

        public string ReadSimpleValue()
        {
            if (isClosed)
                Debug.WriteLine($"Connection {this.GetHashCode()} closed");
            else
                Debug.WriteLine($"Connection {this.GetHashCode()} returning {_content.SimpleValue}");


            return _content.SimpleValue;
        }

        internal List<string> SimpleList => _content.SimpleList;


        internal void SetSimpleValue(string value)
        {
            Debug.WriteLine($"Connection {this.GetHashCode()} setting SimpleValue to {value}");

            this._content.SimpleValue = value;
        }
    }
}