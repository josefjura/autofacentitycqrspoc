﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class MemoryDbRepository : IRepository
    {
        public MemoryDbRepository(MemoryDbConnection conn)
        {
            this.Connection = conn;
        }

        public MemoryDbConnection Connection { get; private set; }

        public void SetSimpleValue(string value)
        {
            Connection.SetSimpleValue(value);
        }
    }
}
