﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class MemoryDbReadModel : IReadModel
    {

        public MemoryDbReadModel(MemoryDbConnection conn)
        {
            this.Connection = conn;
        }

        public MemoryDbConnection Connection { get; private set; }
        public string SimpleValue => Connection.ReadSimpleValue();

        public IQueryable<string> SimpleList => Connection.SimpleList.AsQueryable();

        public bool CheckConnection()
        {
            return true;
        }
    }
}
