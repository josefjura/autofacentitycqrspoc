﻿using AutofacEntityCqrsPoc.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class MemoryDbListRepository : IListRepository
    {
        public MemoryDbListRepository(MemoryDbConnection conn)
        {
            this.Connection = conn;
        }

        public MemoryDbConnection Connection { get; private set; }

        public void Insert(string value)
        {
            Connection.SimpleList.Add(value);
        }

        public void InsertRange(IEnumerable<string> values)
        {
            Connection.SimpleList.AddRange(values);
        }
    }
}
