﻿using System.Collections.Generic;
using System.Linq;

namespace AutofacEntityCqrsPoc
{
    public interface IReadModel
    {
        string SimpleValue { get; }
        IQueryable<string> SimpleList { get; }

        bool CheckConnection();
    }
}