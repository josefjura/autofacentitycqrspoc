﻿using System.Collections.Generic;

namespace AutofacEntityCqrsPoc
{
    public interface IListRepository
    {
        void Insert(string value);
        void InsertRange(IEnumerable<string> values);
    }
}