﻿using AutofacEntityCqrsPoc.DAL.LocalDb.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class LocalDbRepository : IRepository
    {
        private ISimpleContext _ctx;

        public LocalDbRepository(ISimpleContext ctx)
        {
            this._ctx = ctx;
        }

        public void SetSimpleValue(string value)
        {
            var entity = _ctx.Specials.FirstOrDefault(x => x.Key == LocalDbDictionary.SimpleValueKey);

            if (entity == null)
            {
                _ctx.Specials.Add(new SpecialValue { Key = LocalDbDictionary.SimpleValueKey, Value = value });
            }
            else
            {
                entity.Value = value;
            }

            _ctx.SaveChanges();
        }
    }
}
