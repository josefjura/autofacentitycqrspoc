﻿using AutofacEntityCqrsPoc.Commands;
using AutofacEntityCqrsPoc.DAL.LocalDb.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class LocalDbListRepository : IListRepository
    {
        private ISimpleContext _ctx;

        public LocalDbListRepository(ISimpleContext ctx)
        {
            this._ctx = ctx;
        }

        public void Insert(string value)
        {
            _ctx.Items.Add(new Item { Value = value });
            _ctx.SaveChanges();
        }

        public void InsertRange(IEnumerable<string> values)
        {
            _ctx.Items.AddRange(values.Select(x => new Item() { Value = x }));
            _ctx.SaveChanges();
        }
    }
}
