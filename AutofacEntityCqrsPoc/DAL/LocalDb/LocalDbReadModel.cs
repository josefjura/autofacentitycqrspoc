﻿using AutofacEntityCqrsPoc.DAL.LocalDb.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class LocalDbReadModel : IReadModel
    {
        private ISimpleContext _ctx;

        public LocalDbReadModel(ISimpleContext ctx)
        {
            this._ctx = ctx;
        }

        public string SimpleValue => _ctx.Specials.FirstOrDefault(x => x.Key == LocalDbDictionary.SimpleValueKey)?.Value ?? null;

        public IQueryable<string> SimpleList => _ctx.Items.Select(x => x.Value);

        public bool CheckConnection()
        {
            return _ctx.IsConnected;
        }
    }
}
