﻿namespace AutofacEntityCqrsPoc.DAL.LocalDb.Context
{
    public class Item
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}