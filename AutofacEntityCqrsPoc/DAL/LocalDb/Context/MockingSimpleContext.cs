﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc.DAL.LocalDb.Context
{
    public class MockingSimpleContext : ISimpleContext
    {
        private Mock<ISimpleContext> _context;

        public MockingSimpleContext(Mock<ISimpleContext> mockContext)
        {
            this.Context = mockContext;
        }

        public DbSet<Item> Items { get => Context.Object.Items; }

        public DbSet<SpecialValue> Specials { get => Context.Object.Specials; }
        public Mock<ISimpleContext> Context { get => _context; set => _context = value; }

        public bool IsConnected { get => Context.Object.IsConnected; }

        public int SaveChanges()
        {
            return Context.Object.SaveChanges();
        }
    }
}
