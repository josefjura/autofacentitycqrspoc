﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc.DAL.LocalDb.Context
{
    public interface ISimpleContext
    {
        DbSet<Item> Items { get; }
        DbSet<SpecialValue> Specials { get; }
        bool IsConnected { get; }

        int SaveChanges();
    }
}
