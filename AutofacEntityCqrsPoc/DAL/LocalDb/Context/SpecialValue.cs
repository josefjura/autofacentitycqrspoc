﻿using System.ComponentModel.DataAnnotations;

namespace AutofacEntityCqrsPoc.DAL.LocalDb.Context
{
    public class SpecialValue
    {
        public string Value { get; set; }

        [Key]
        public string Key { get; set; }
    }
}