﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    class Program
    {
        static void Main(string[] args)
        {
            var cb = new ContainerBuilder();
            Bootstrap.RegisterMemoryDb(cb, new MemoryDbContent
            {
                SimpleValue = "Old SimpleValue",
                SimpleList = new List<string> { "First", "Second" }
            });

            var cnt = Bootstrap.BulkRegister(cb);

            var module = new PrismModule(cnt.Resolve<IBusinessService>());

            module.InitializeValues();

            module.ChangeSimpleValue("New SimpleValue");

            Console.ReadLine();
        }
    }
}
