﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public static class Extensions
    {
        public static IEnumerable<TView> MapEnumerable<TDomainModel, TView>(this IEnumerable<TDomainModel> domainEnumerable)
            where TDomainModel : class
            where TView : class
        {
            return AutoMapper.Mapper.Map<IEnumerable<TDomainModel>, IEnumerable<TView>>(domainEnumerable);
        }

        public static List<TView> MapEnumerableToList<TDomainModel, TView>(this IEnumerable<TDomainModel> domainEnumerable)
    where TDomainModel : class
    where TView : class
        {
            return domainEnumerable.MapEnumerable<TDomainModel, TView>().ToList();
        }
    }
}
