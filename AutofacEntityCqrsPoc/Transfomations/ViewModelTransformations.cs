﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc.Transfomations
{
    public static class SimpleListItemViewModelTransformations
    {
        public static SimpleListItemViewModel ToViewModel(this string model)
        {
            return Mapper.Map<string, SimpleListItemViewModel>(model);            
        }

        public static List<SimpleListItemViewModel> ToViewModel(this List<string> models)
        {
            return models.MapEnumerableToList<string, SimpleListItemViewModel>();
        }
    }

    public static class SimpleValueViewModelTransformations
    {
        public static SimpleValueViewModel ToViewModel(this string model)
        {
            return Mapper.Map<string, SimpleValueViewModel>(model);
        }
    }
}
