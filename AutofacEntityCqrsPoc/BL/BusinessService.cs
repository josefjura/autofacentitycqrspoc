﻿using Autofac;
using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class BusinessService : IBusinessService
    {
        private IQueryDispatcher qd;
        private ICommandDispatcher cd;

        public BusinessService(IQueryDispatcher qDis, ICommandDispatcher cDis)
        {
            this.qd = qDis;
            this.cd = cDis;
        }

        public Result<bool> CheckConnectionToStore()
        {
            var result = qd.Execute<StoreStatusQuery, bool>(new StoreStatusQuery());

            return Result.Create(result);
        }

        public Result<List<string>> InsertManyToSimpleList(IEnumerable<string> value)
        {
            cd.Execute(new InsertManyToSimpleListCommand(value));

            return ReadSimpleList();
        }

        public Result<List<string>> InsertToSimpleList(string value)
        {
            cd.Execute(new InsertToSimpleListCommand(value));

            return ReadSimpleList();
        }

        public Result<List<string>> ReadSimpleList()
        {
            var result = qd.Execute<SimpleListQuery, List<string>>(new SimpleListQuery());

            return Result.Create(result);
        }

        public Result<string> ReadSimpleValue()
        {
            string result = qd.Execute<SimpleValueQuery, string>(new SimpleValueQuery());

            return Result.Create(result);
        }

        public Result<string> UpdateSimpleValue(string value)
        {
            cd.Execute(new SetSimpleValueCommand(value));

            return ReadSimpleValue();
        }
    }
}
