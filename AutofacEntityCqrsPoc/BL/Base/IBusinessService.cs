﻿using System.Collections.Generic;

namespace AutofacEntityCqrsPoc
{
    public interface IBusinessService
    {
        Result<bool> CheckConnectionToStore();
        Result<string> UpdateSimpleValue(string value);
        Result<string> ReadSimpleValue();
        Result<List<string>> ReadSimpleList();
        Result<List<string>> InsertToSimpleList(string value);
        Result<List<string>> InsertManyToSimpleList(IEnumerable<string> value);
    }
}