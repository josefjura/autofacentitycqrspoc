﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{

    [Serializable]
    public class MissingHandlerException : Exception
    {
        private Type InputType;
        private Type OutputType;

        public MissingHandlerException() { }
        public MissingHandlerException(string message, Type inputType, Type outputType = null) : base(message) {
            this.InputType = inputType;
            this.OutputType = outputType;
        }
        public MissingHandlerException(string message, Exception inner, Type inputType, Type outputType = null) : base(message, inner) {
            this.InputType = inputType;
            this.OutputType = outputType;
        }

        protected MissingHandlerException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
