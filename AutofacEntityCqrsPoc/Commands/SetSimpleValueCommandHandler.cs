﻿namespace AutofacEntityCqrsPoc
{
    public class SetSimpleValueCommandHandler : ICommandHandler<SetSimpleValueCommand>
    {
        private IRepository _repo;

        public SetSimpleValueCommandHandler(IRepository repo)
        {
            this._repo = repo;
        }

        public void Execute(SetSimpleValueCommand command)
        {
            _repo.SetSimpleValue(command.Value);
        }
    }
}
