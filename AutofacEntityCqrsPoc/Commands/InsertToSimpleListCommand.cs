﻿namespace AutofacEntityCqrsPoc
{
    public class InsertToSimpleListCommand : ICommand
    {
        private string value;

        public InsertToSimpleListCommand(string value)
        {
            this.Value = value;
        }

        public string Value { get => value; set => this.value = value; }
    }
}