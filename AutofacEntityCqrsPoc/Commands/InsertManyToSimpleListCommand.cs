﻿using System.Collections.Generic;

namespace AutofacEntityCqrsPoc
{
    public class InsertManyToSimpleListCommand : ICommand
    {
        private IEnumerable<string> values;

        public InsertManyToSimpleListCommand(IEnumerable<string> values)
        {
            this.Values = values;
        }

        public IEnumerable<string> Values { get => values; set => values = value; }
    }
}