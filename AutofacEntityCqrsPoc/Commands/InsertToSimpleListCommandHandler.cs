﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc.Commands
{
    public class InsertToSimpleListCommandHandler : ICommandHandler<InsertToSimpleListCommand>
    {
        private IListRepository _repo;

        public InsertToSimpleListCommandHandler(IListRepository repo)
        {
            this._repo = repo;
        }

        public void Execute(InsertToSimpleListCommand command)
        {
            _repo.Insert(command.Value);
        }
    }
}
