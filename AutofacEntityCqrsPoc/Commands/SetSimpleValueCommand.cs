﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class SetSimpleValueCommand : ICommand
    {
        public string Value { get; private set; }

        public SetSimpleValueCommand(string value)
        {
            this.Value = value;
        }
    }
}
