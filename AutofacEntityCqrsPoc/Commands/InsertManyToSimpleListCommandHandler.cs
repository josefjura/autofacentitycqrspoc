﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc.Commands
{
    public class InsertManyToSimpleListCommandHandler : ICommandHandler<InsertManyToSimpleListCommand>
    {
        private IListRepository _repo;

        public InsertManyToSimpleListCommandHandler(IListRepository repo)
        {
            this._repo = repo;
        }

        public void Execute(InsertManyToSimpleListCommand command)
        {
            _repo.InsertRange(command.Values);
        }
    }
}
