﻿using Autofac;
using Autofac.Core.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class AutofacLifetimeCommandDispatcher : ICommandDispatcher
    {
        private ILifetimeScope _scope;

        public AutofacLifetimeCommandDispatcher(ILifetimeScope scope)
        {
            this._scope = scope;
        }

        public void Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            using (var scope = _scope.BeginLifetimeScope())
            {
                try
                {
                    var handler = scope.Resolve<ICommandHandler<TCommand>>();

                    handler.Execute(command);
                }
                catch (ComponentNotRegisteredException ex)
                {
                    throw new MissingHandlerException("Missing command handler", ex, typeof(TCommand));
                }
            }
        }
    }
}
