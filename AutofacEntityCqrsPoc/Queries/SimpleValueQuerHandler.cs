﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class SimpleValueQuerHandler : IQueryHandler<SimpleValueQuery, string>
    {
        private IReadModel _readModel;

        public SimpleValueQuerHandler(IReadModel readModel)
        {
            this._readModel = readModel;
        }

        public string Execute(SimpleValueQuery query)
        {
            return _readModel.SimpleValue;
        }
    }
}
