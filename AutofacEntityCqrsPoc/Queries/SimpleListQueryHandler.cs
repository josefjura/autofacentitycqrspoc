﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class SimpleListQueryHandler : IQueryHandler<SimpleListQuery, List<string>>
    {
        private IReadModel _readModel;

        public SimpleListQueryHandler(IReadModel readModel)
        {
            this._readModel = readModel;
        }

        public List<String> Execute(SimpleListQuery query)
        {
            return _readModel.SimpleList.ToList();
        }
    }
}
