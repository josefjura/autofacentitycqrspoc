﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class StoreStatusQueryHandler : IQueryHandler<StoreStatusQuery, bool>
    {
        private IReadModel _readModel;

        public StoreStatusQueryHandler(IReadModel readModel)
        {
            this._readModel = readModel;
        }

        public bool Execute(StoreStatusQuery query)
        {
            return _readModel.CheckConnection();
        }
    }
}
