﻿using Autofac;
using Autofac.Core.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class AutofacLifetimeQueryDispatcher : IQueryDispatcher
    {
        private ILifetimeScope _scope;

        public AutofacLifetimeQueryDispatcher(ILifetimeScope scope)
        {
            this._scope = scope;
        }

        public TResult Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>
        {
            using (var scope = _scope.BeginLifetimeScope())
            {
                try
                {
                    var handler = scope.Resolve<IQueryHandler<TQuery, TResult>>();

                    return handler.Execute(query);
                }
                catch (ComponentNotRegisteredException ex)
                {
                    throw new MissingHandlerException("Missing query handler", ex, typeof(TQuery), typeof(TResult));
                }
            }
        }
    }
}
