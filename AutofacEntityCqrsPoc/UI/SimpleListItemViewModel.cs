﻿namespace AutofacEntityCqrsPoc
{
    public class SimpleListItemViewModel
    {       
        public SimpleListItemViewModel(string x)
        {
            this.Value = x;
        }

        public string Value { get; internal set; }
    }
}