﻿namespace AutofacEntityCqrsPoc
{
    public class SimpleValueViewModel
    {
        private string _value;

        public SimpleValueViewModel(string value)
        {
            Value = value;
        }

        public string Value { get => _value; set => _value = value; }
    }
}