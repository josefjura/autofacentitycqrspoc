﻿using Autofac;
using Autofac.Core;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AutofacEntityCqrsPoc
{
    public class PrismModule
    {
        private IBusinessService _service;

        public SimpleValueViewModel SimpleValue { get; set; }
        public List<SimpleListItemViewModel> SimpleList { get; set; }
        public bool IsStoreConnected
        {
            get
            {
                return storeState;
            }
            set
            {
                storeState = value;
                // There should be an property changed call, do not care in scope of this PoC
            }
        }

        /// <summary>
        /// Might be updated periodically somehow, do not care in scope of this PoC
        /// </summary>
        public void UpdateState()
        {
            IsStoreConnected = _service.CheckConnectionToStore().Value;
        }

        public PrismModule(IBusinessService service)
        {
            _service = service;
            SimpleList = new List<SimpleListItemViewModel>();
        }

        public void ChangeSimpleValue(string SimpleValue)
        {
            this.SimpleValue = Mapper.Map<string, SimpleValueViewModel>(_service.UpdateSimpleValue(SimpleValue).Value);
        }

        public void InitializeValues()
        {
            SimpleValue = Mapper.Map<string, SimpleValueViewModel>(_service.ReadSimpleValue().Value);
            SimpleList = _service.ReadSimpleList().Value.MapEnumerableToList<string, SimpleListItemViewModel>();
            UpdateState();
        }

        public void AddToSimpleList(string value)
        {
            SimpleList = _service.InsertToSimpleList(value).Value.MapEnumerableToList<string, SimpleListItemViewModel>();
        }

        public void AddManyToSimpleList(IEnumerable<string> values)
        {
            SimpleList = _service.InsertManyToSimpleList(values).Value.MapEnumerableToList<string, SimpleListItemViewModel>();
        }

        private bool storeState = false;
    }
}