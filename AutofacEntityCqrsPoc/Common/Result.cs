﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    /// <summary>
    /// Wanna-Be-Sure class, probably will be removed
    /// </summary>
    public class Result
    {
        public static Result<T> Create<T>(T value)
        {
            return new Result<T>(value);
        }
    }

    /// <summary>
    /// Wanna-Be-Sure class, probably will be removed
    /// </summary>
    public class Result<T> : Result
    {
        public Result(T value)
        {
            Value = value;
        }

        public T Value { get; set; }

        public static Result<T> Create(T value)
        {
            return new Result<T>(value);
        }
    }
}
