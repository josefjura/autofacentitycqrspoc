﻿using Autofac;
using Autofac.Core;
using AutofacEntityCqrsPoc.DAL.LocalDb.Context;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutofacEntityCqrsPoc
{
    public class Bootstrap
    {

        public static void RegisterCommandHandlers(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
            .As(t => t.GetInterfaces()
            .Where(i => i.IsClosedTypeOf(typeof(ICommandHandler<>)))
            .Select(i => i));
        }

        public static void RegisterQueryHandlers(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
            .As(t => t.GetInterfaces()
            .Where(i => i.IsClosedTypeOf(typeof(IQueryHandler<,>)))
            .Select(i => i));
        }

        public static void RegisterMemoryDb(ContainerBuilder builder, MemoryDbContent initInstance)
        {
            builder.RegisterType<MemoryDbConnection>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<MemoryDbRepository>().As<IRepository>().InstancePerLifetimeScope();
            builder.RegisterType<MemoryDbListRepository>().As<IListRepository>().InstancePerLifetimeScope();
            builder.RegisterType<MemoryDbReadModel>().As<IReadModel>();
            builder.RegisterInstance(initInstance).As<MemoryDbContent>();
        }

        public static void RegisterLocalDb(ContainerBuilder builder)
        {
            builder.RegisterType<LocalDbRepository>().As<IRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LocalDbListRepository>().As<IListRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LocalDbReadModel>().As<IReadModel>();
        }

        public static IContainer BulkRegister(ContainerBuilder builder)
        {

            builder.RegisterType<BusinessService>().As<IBusinessService>();
            builder.RegisterType<AutofacLifetimeCommandDispatcher>().As<ICommandDispatcher>();
            builder.RegisterType<AutofacLifetimeQueryDispatcher>().As<IQueryDispatcher>();
            RegisterCommandHandlers(builder);
            RegisterQueryHandlers(builder);

            return builder.Build();
        }


        public static void SetupMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<string, SimpleListItemViewModel>().ConvertUsing(x => new SimpleListItemViewModel(x));
                cfg.CreateMap<SimpleListItemViewModel, string>().ConvertUsing(x => x.Value);
                cfg.CreateMap<string, SimpleValueViewModel>().ConvertUsing(x => new SimpleValueViewModel(x));
                cfg.CreateMap<SimpleValueViewModel, string>().ConvertUsing(x => x.Value);
            });

            Mapper.AssertConfigurationIsValid();
        }

    }
}
