﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Autofac;
using System.Collections.Generic;

namespace AutofacEntityCqrsPoc.Tests
{
    [TestClass]
    public class PrismModuleMemoryDbTests
    {
        private readonly string initSimpleValue = "Old SimpleValue";
        private readonly string firstItem = "First";
        private readonly string secondItem = "Second";
        private readonly List<string> initSimpleList;

        public IContainer Container { get; private set; }

        public PrismModuleMemoryDbTests()
        {
            initSimpleList = new List<string>() { firstItem, secondItem };

            var cb = new ContainerBuilder();
            Bootstrap.RegisterMemoryDb(cb, new MemoryDbContent
            {
                SimpleValue = initSimpleValue,
                SimpleList = initSimpleList
            });

            Container = Bootstrap.BulkRegister(cb);

            Bootstrap.SetupMapper();
        }

        [TestInitialize]
        public void Init()
        {

        }

        [TestMethod]
        public void Memory_SimpleValueUpdate()
        {
            var updatedValue = "New SimpleValue";

            var module = new PrismModule(Container.Resolve<IBusinessService>());

            Assert.IsNull(module.SimpleValue);

            module.InitializeValues();

            Assert.AreEqual(initSimpleValue, module.SimpleValue.Value);

            module.ChangeSimpleValue(updatedValue);

            Assert.AreEqual(updatedValue, module.SimpleValue.Value);
        }

        [TestMethod]
        public void Memory_ResolveModule()
        {
            var module = new PrismModule(Container.Resolve<IBusinessService>());

            Assert.IsNull(module.SimpleValue);
        }

        [TestMethod]
        public void Memory_SimpleListInit()
        {
            var module = new PrismModule(Container.Resolve<IBusinessService>());

            module.InitializeValues();

            Assert.AreEqual(initSimpleList.Count, module.SimpleList.Count);
            Assert.AreEqual(firstItem, module.SimpleList[0].Value);
            Assert.AreEqual(secondItem, module.SimpleList[1].Value);
        }

        [TestMethod]
        public void Memory_SimpleListInsert()
        {
            var thirdItem = "Third";

            var module = new PrismModule(Container.Resolve<IBusinessService>());

            module.InitializeValues();

            module.AddToSimpleList(thirdItem);

            Assert.AreEqual(3, module.SimpleList.Count);
            Assert.AreEqual(thirdItem, module.SimpleList[2].Value);
        }

        [TestMethod]
        public void Memory_SimpleListInsertMany()
        {

            var thirdItem = "Third";
            var fourthItem = "Fourth";

            var module = new PrismModule(Container.Resolve<IBusinessService>());

            module.InitializeValues();

            module.AddManyToSimpleList(new[] { thirdItem, fourthItem });

            Assert.AreEqual(4, module.SimpleList.Count);
            Assert.AreEqual(thirdItem, module.SimpleList[2].Value);
            Assert.AreEqual(fourthItem, module.SimpleList[3].Value);
        }

        [TestMethod]
        public void Memory_StatusQuery()
        {
            var module = new PrismModule(Container.Resolve<IBusinessService>());

            Assert.IsFalse(module.IsStoreConnected);

            module.InitializeValues();

            Assert.IsTrue(module.IsStoreConnected);
        }
    }
}
