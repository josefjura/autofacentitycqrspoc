﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Autofac;
using System.Collections.Generic;
using AutofacEntityCqrsPoc.DAL.LocalDb.Context;
using System.Linq;
using Moq;
using System.Data.Entity;

namespace AutofacEntityCqrsPoc.Tests
{
    [TestClass]
    public class PrismModuleMockingLocalDbTests
    {
        private readonly string initSimpleValue = "Old SimpleValue";
        private readonly string firstItem = "First";
        private readonly string secondItem = "Second";
        private readonly List<string> initSimpleList;

        public IContainer Container { get; private set; }

        public MockingSimpleContext Mock { get; private set; }
        public Mock<DbSet<Item>> ItemsSet { get; private set; }
        public Mock<DbSet<SpecialValue>> SpecialsSet { get; private set; }

        public PrismModuleMockingLocalDbTests()
        {
            initSimpleList = new List<string>() { firstItem, secondItem };

            var cb = new ContainerBuilder();
            Bootstrap.SetupMapper();
            Bootstrap.RegisterLocalDb(cb);
            var items = initSimpleList.Select(x => new Item { Value = x }).ToList();
            var specials = new List<SpecialValue> { new SpecialValue { Key = LocalDbDictionary.SimpleValueKey, Value = initSimpleValue } };

            this.ItemsSet = new Mock<DbSet<Item>>().SetupData(items);
            this.SpecialsSet = new Mock<DbSet<SpecialValue>>().SetupData(specials);

            var mockContext = new Mock<ISimpleContext>();
            mockContext.Setup(m => m.Items).Returns(ItemsSet.Object);
            mockContext.Setup(m => m.Specials).Returns(SpecialsSet.Object);
            mockContext.Setup(m => m.IsConnected).Returns(true);

            this.Mock = new MockingSimpleContext(mockContext);

            cb.RegisterInstance(Mock).As<ISimpleContext>();
            Container = Bootstrap.BulkRegister(cb);
        }

        [TestInitialize]
        public void Init()
        {

        }

        [TestMethod]
        public void Local_SimpleValueUpdate()
        {
            var updatedValue = "New SimpleValue";

            var module = new PrismModule(Container.Resolve<IBusinessService>());

            Assert.IsNull(module.SimpleValue);

            module.InitializeValues();

            Assert.AreEqual(initSimpleValue, module.SimpleValue.Value);

            module.ChangeSimpleValue(updatedValue);

            Assert.AreEqual(updatedValue, module.SimpleValue.Value);

            Mock.Context.Verify(m => m.SaveChanges(), Times.Once);
        }

        [TestMethod]
        public void Local_ResolveModule()
        {
            var module = new PrismModule(Container.Resolve<IBusinessService>());

            Assert.IsNull(module.SimpleValue);
        }

        [TestMethod]
        public void Local_SimpleListInit()
        {
            var module = new PrismModule(Container.Resolve<IBusinessService>());

            module.InitializeValues();

            Assert.AreEqual(initSimpleList.Count, module.SimpleList.Count);
            Assert.AreEqual(firstItem, module.SimpleList[0].Value);
            Assert.AreEqual(secondItem, module.SimpleList[1].Value);

            Mock.Context.Verify(m => m.SaveChanges(), Times.Never);
        }

        [TestMethod]
        public void Local_SimpleListInsert()
        {
            var thirdItem = "Third";

            var module = new PrismModule(Container.Resolve<IBusinessService>());

            module.InitializeValues();

            module.AddToSimpleList(thirdItem);

            Assert.AreEqual(3, module.SimpleList.Count);
            Assert.AreEqual(thirdItem, module.SimpleList[2].Value);

            ItemsSet.Verify(m => m.Add(It.IsAny<Item>()), Times.Once);
            Mock.Context.Verify(m => m.SaveChanges(), Times.Once);
        }

        [TestMethod]
        public void Local_SimpleListInsertMany()
        {

            var thirdItem = "Third";
            var fourthItem = "Fourth";

            var module = new PrismModule(Container.Resolve<IBusinessService>());

            module.InitializeValues();

            module.AddManyToSimpleList(new[] { thirdItem, fourthItem });

            Assert.AreEqual(4, module.SimpleList.Count);
            Assert.AreEqual(thirdItem, module.SimpleList[2].Value);
            Assert.AreEqual(fourthItem, module.SimpleList[3].Value);

            ItemsSet.Verify(m => m.Add(It.IsAny<Item>()), Times.Never);
            ItemsSet.Verify(m => m.AddRange(It.IsAny<IEnumerable<Item>>()), Times.Once);
            Mock.Context.Verify(m => m.SaveChanges(), Times.Once);
        }

        [TestMethod]
        public void Local_StatusQuery()
        {
            var module = new PrismModule(Container.Resolve<IBusinessService>());

            Assert.IsFalse(module.IsStoreConnected);

            module.InitializeValues();

            Assert.IsTrue(module.IsStoreConnected);

            Mock.Context.Verify(m => m.IsConnected, Times.Once);
            Mock.Context.Verify(m => m.SaveChanges(), Times.Never);
        }
    }
}
